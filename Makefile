# crypt-edit (including this Makefile) is released into the public domain by
# it's copyright holders.

ifeq (${DESTDIR}${PREFIX},)
	PREFIX = ${HOME}/.local
	ifeq (0,$(shell id -u))
		PREFIX = /usr/local
	endif
endif
app_name = crypt-edit
exe_name = ${app_name}
app_dir = ${DESTDIR}${PREFIX}/share/${app_name}
bin_dir = ${DESTDIR}${PREFIX}/bin

install:
	install -d ${app_dir}
	install -m 644 Makefile README.md ${app_dir}
	install -m 755 ${exe_name}.bash ${app_dir}
	install -d ${bin_dir}
	ln -sf ${app_dir}/${exe_name}.bash ${bin_dir}/${exe_name}
	echo "DESTDIR=${DESTDIR}" >> ${app_dir}/install-vars.info
	echo "PREFIX=${PREFIX}" >> ${app_dir}/install-vars.info
	echo "app_name=${app_name}" >> ${app_dir}/install-vars.info
	echo "app_dir=${app_dir}" >> ${app_dir}/install-vars.info
	echo "bin_dir=${bin_dir}" >> ${app_dir}/install-vars.info

uninstall:
	rm ${bin_dir}/${exe_name}
	rm ${app_dir}/*
	rmdir -p ${bin_dir} ${app_dir} >/dev/null 2>&1 || true

update:
	if [ -d .git ]; then git pull; else echo "Not installed via git!" >&2; fi

