# crypt-edit

A simple wrapper around `gpg` and your favorite (text-)editor.

## Install

Currently there are three possible ways how you can install `crypt-edit`:
  1. Copy the `crypt-edit.bash` into one of your PATH directories
  2. Clone the repository and symlink the `crypt-edit.bash` into one of your
  PATH directories
  3. Clone the repository and `make install`

### Notes for the `make install` variant

You can adjust the install location by setting the variables `PREFIX`, `app_dir`
and `bin_dir`.

While the `PREFIX` variable can either be set as environment variable or as make
variable, the `app_dir` and `bin_dir` variables have to be specified as make
variables.  
(e.g.: `make install  app_dir='/my/app/dir'  bin_dir='/my/bin/dir'`)

Default values: (normal user)  
PREFIX: `~/.local`  
app_dir: `~/.local/share/crypt-edit`  
bin_dir: `~/.local/bin`

Default values: (root user)  
PREFIX: `/usr/local`  
app_dir: `/usr/local/share/crypt-edit`  
bin_dir: `/usr/local/bin`

## Usage

```
crypt-edit [--new] FILE_NAME
```

## How it works

crypt-edit simply decrypts the specified file and opens it in your favourite text
editor or `vi` if you don't have any configured. Your Favourite text editor is
specified by either the VISUAL or the EDITOR environment variables (like for
other UNIX tools). After you finished editing the file and close the editor,
crypt-edit automatically re-encrypts the edited temporary file.

## Examples

```
crypt-edit ./my-secret-data.txt.gpg
```

Decrypts the specified file into a temporary clear-text file and opens it with
your favorite text editor. After you finished editing the temporary file and
close the editor application, crypt-edit will automatically re-encrypt the now
edited text file.

```
crypt-edit --new my-new-secret-data-file.txt
```
  
Will start your favorite text editor to allow you to edit a temporary clear-text
file. After you have closed the editor the file will be automatically encrypted.
The resulting file will always have the `.gpg` extension. So in this example the
resulting filename would be: `my-new-secret-data-file.txt.gpg`.

## Copyright

crypt-edit is released into the public domain by it's copyright holders.

This README file was originally written by [Andanan](https://gitlab.com/andanan)
and is likewise released into the public domain.

