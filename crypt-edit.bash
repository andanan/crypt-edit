#!/bin/bash
#
# crypt-edit is released into the public domain by the copyright holders.
#

app_name=$(basename "$0")
if [[ $# -eq 0 ]]; then
	echo "Usage: $app_name [--new] ENCRYPTED_FILE"
	exit 1
fi
case $1 in
--new)
	shift
	isNew=true
;;
-*)
	echo "Unknown option: $1"
	exit 1
;;
esac
file="$1"
if [[ ${isNew,,} == 'true' ]]; then
	if [[ ! $file =~ .*\.gpg ]]; then
		file="${file}.gpg"
	fi
	if [[ -e $file ]]; then
		echo "File already exists: $file"
		exit 1
	fi
elif [[ ! -f $file ]]; then
	echo "File not found: $file" >&2
	exit 1
fi
read -r -s -p 'password: ' pwd 1>&2
echo

tmp_dec=$(mktemp)
tmp_enc=$(mktemp)
on_exit() {
	rm -f "$tmp_dec" "$tmp_enc"
	if [[ -f $tmp_dec ]]; then
		echo "File still exists: $tmp_dec"
		exit 1
	fi
	if [[ -f $tmp_enc ]]; then
		echo "File still exists: $tmp_enc"
		exit 1
	fi
}
trap on_exit EXIT

{
	if [[ $isNew == 'true' ]]; then
		touch "$file" "$tmp_dec"
	else
		gpg --batch --passphrase-fd 0 --decrypt "$file" > "$tmp_dec" <<<"$pwd"
	fi
} && \
"${VISUAL:-${EDITOR:-vi}}" "$tmp_dec" && \
gpg --batch --yes --passphrase-fd 0 --symmetric --output "$tmp_enc" "$tmp_dec" <<<"$pwd" && \
mv "$tmp_enc" "$file"

